# gptray

## About
ChatGPT cross platform (Windows, Linux, OSX) desktop app with system tray icon.
Has a "memory" which consists of a conversation history that is truncated and summarized as needed. 

![Example](gptray.png)

## Misc Features
- Config file is stored in your home directory
- Uses your own openai API key
- Supports dark and light mode
- Auto copies GPT responses to the clipboard 
- System tray icon so it's always handy
- Resizeable window
- Basic (TTS) text to speech using `espeak` on linux and `say` on OSX (this is a config setting)


## Build

### Linux
```go build -o GPTray main.go bundled.go```

### OSX
 ```GOOS=darwin GOARCH=amd64 go build -o GPTray main.go bundled.go```

- To create OSX app 

```fyne package --executable GPTray --icon bender.png --name GPTray``` 


### Windows
``` meh ```

## Run
- Run the GPTray executable. 
- In click the icon in the systemtray -> click Chat
- If the API Key isn't set it will ask for it and verify the key. Once verified the chat window will open

## Issues
There's a bunch of them. 