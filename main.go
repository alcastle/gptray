package main

import (
	"context"
	"image/color"
	"log"
	"net/url"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"unicode/utf8"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/driver/desktop"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"golang.design/x/clipboard"

	"github.com/fsnotify/fsnotify"
	openai "github.com/sashabaranov/go-openai"
	"github.com/spf13/viper"
)

var (
	a         fyne.App
	menuItem1 *fyne.MenuItem
	menu      *fyne.Menu
	apiKey    string
	home      string
	tmpMemory []openai.ChatCompletionMessage
)

const (
	getAPIDocs          = "https://help.openai.com/en/articles/4936850-where-do-i-find-my-secret-api-key"
	historyFileName     = ".GPTray_history_all"
	historyMineFIlename = ".GPTray_history_mine"
	configFileName      = ".GPTray_config"
	configAppName       = "GPTray"
	myName              = "Me"
	botName             = "GPT"
	messageIndent       = 20
	version             = "0.7"
	appID               = "com.castleindustries.gptray"
	GPT_MODEL           = "gpt-3.5-turbo-16k"
)

type Chat struct {
	widget.Entry
}
type message struct {
	widget.BaseWidget
	text, from string
}
type ChatEntry struct {
	widget.Entry
}
type myTheme struct {
	fyne.Theme
}

func (m *myTheme) Color(n fyne.ThemeColorName, v fyne.ThemeVariant) color.Color {
	switch n {
	case theme.ColorNameBackground:
		if v == theme.VariantLight {
			//return &color.NRGBA{0xcf, 0xd8, 0xdc, 0xff}
			return &color.NRGBA{0xcf, 0xd8, 0xdc, 0xff}
		}
		return &color.NRGBA{0x45, 0x5A, 0x64, 0xff}
	case theme.ColorNameFocus:
		return &color.NRGBA{0xff, 0xc1, 0x07, 0xff}
	}

	return theme.DefaultTheme().Color(n, v)
}
func (m *myTheme) Size(n fyne.ThemeSizeName) float32 {
	return theme.DefaultTheme().Size(n)
}
func (m *myTheme) Font(n fyne.TextStyle) fyne.Resource {
	//theme.DefaultTextBoldFont()
	return theme.DefaultTheme().Font(n)
}
func (m *myTheme) Icon(n fyne.ThemeIconName) fyne.Resource {
	return theme.DefaultTheme().Icon(n)
}

func createMessage(text, name string) *message {
	m := &message{text: text, from: name}
	m.ExtendBaseWidget(m)

	return m
}

type messageRender struct {
	msg *message
	bg  *canvas.Rectangle
	txt *widget.Label
}

func (r *messageRender) messageMinSize(s fyne.Size) fyne.Size {
	fitSize := s.Subtract(fyne.NewSize(messageIndent, 0))
	r.txt.Resize(fitSize.Max(r.txt.MinSize()))
	return r.txt.MinSize()
}
func (r *messageRender) MinSize() fyne.Size {
	itemSize := r.messageMinSize(r.msg.Size())
	indent := fyne.NewSize(messageIndent, 0)
	return itemSize.Add(indent)
}
func (r *messageRender) Layout(s fyne.Size) {
	itemSize := r.messageMinSize(s)
	itemSize = itemSize.Max(fyne.NewSize(s.Width-messageIndent, s.Height))
	bgPos := fyne.NewPos(0, 0)
	if r.msg.from == myName {
		r.txt.Alignment = fyne.TextAlignTrailing
		r.txt.TextStyle = fyne.TextStyle{Bold: true}
		//r.bg.FillColor = theme.PrimaryColorNamed(theme.ColorBlue)
		//r.bg.FillColor = color.NRGBA{0xff, 0xad, 0xd8, 0xe6} //this is pink -_-
		//r.bg.FillColor = color.RGBA{106, 175, 255, 1} // this renders a white
		//r.bg.FillColor = color.RGBAModel.Convert(color.RGBA{106, 175, 255, 1})
		// r1,g,b,a := 106,175,255,1
		// colorRGBA := color.RGBA{uint8(r1), uint8(g), uint8(b),uint8(a)}
		// colorNRGBA := colorRGBA.
		r.bg.FillColor = color.NRGBA{0x69, 0xAF, 0xff, 0xff} // this provides the blue but doesn't looks great

		// r.bg.FillColor = color.NRGBA{0x38, 0x87, 0xc1, 0xff}
		bgPos = fyne.NewPos(s.Width-itemSize.Width, 0)
	} else {
		r.txt.Alignment = fyne.TextAlignLeading
		r.txt.TextStyle = fyne.TextStyle{Bold: true}
		//r.bg.FillColor = color.NRGBA{0x45, 0x5A, 0x64, 0xff}
		r.bg.FillColor = theme.PrimaryColorNamed(theme.ColorGray)
	}
	r.txt.Move(bgPos)
	r.bg.Resize(itemSize)
	r.bg.Move(bgPos)
}
func (r *messageRender) BackgroundColor() color.Color {
	return color.Transparent
}
func (r *messageRender) Destroy() {
}
func (r *messageRender) Objects() []fyne.CanvasObject {
	return []fyne.CanvasObject{r.bg, r.txt}
}
func (r *messageRender) Refresh() {
}
func (m *message) CreateRenderer() fyne.WidgetRenderer {
	// With Canvas you can set colors, but not wrap (╯°□°)╯︵ ┻━┻
	//text := canvas.NewText(m.text, color.White)
	text := widget.NewLabel(m.text)
	text.Wrapping = fyne.TextWrapWord
	return &messageRender{msg: m, bg: &canvas.Rectangle{}, txt: text}
}
func loadMessages() *fyne.Container {
	return container.NewVBox(
		createMessage("Chat-GPT reporting for duty!", botName),
	)
}
func Explode(delimiter, text string) []string {
	if len(delimiter) > len(text) {
		return strings.Split(delimiter, text)
	} else {
		return strings.Split(text, delimiter)
	}
}

// Retrieve history to date, get it summarized
func summarizeHistory(filename string) string {
	fullpathFile := home + "/." + configAppName + "/" + filename
	fullHistory, _ := os.ReadFile(fullpathFile)
	log.Println(len(fullHistory))
	var charCount int = utf8.RuneCountInString(string(fullHistory))
	var ratio float32 = 4.2
	var maxTokens int = 4097
	var isSmallEnough bool = float32(charCount)/float32(maxTokens) <= ratio
	var length int = len(fullHistory)

	//go resizeHistory(isSmallEnough, length, fullHistory, charCount, maxTokens, ratio)
	for !isSmallEnough {
		if length > 2000 {
			// trim the first 1000 characters from the start of the file
			fullHistory = fullHistory[1000:length]
		}
		// recalculate
		charCount = utf8.RuneCountInString(string(fullHistory))
		isSmallEnough = float32(charCount)/float32(maxTokens) <= ratio
		length = len(fullHistory)

		if length > 10 && isSmallEnough {
			mesg := []openai.ChatCompletionMessage{}
			mesg = append(mesg, openai.ChatCompletionMessage{Role: "user", Content: "Summarize the following and make note of any nouns or pronouns: \"" + string(fullHistory) + "\""})
			resp, _ := apiChatCompletionV2(mesg)
			return resp
		}
	}

	return ""
}

func getHistoryV2() []openai.ChatCompletionMessage {
	var summary = summarizeHistory(historyFileName)
	if len(summary) > 0 {
		tmpMemory = append(tmpMemory, openai.ChatCompletionMessage{Role: "system", Content: "Our conversation history is: '" + summary + "'"})
	}
	tmpMemory = append(tmpMemory, openai.ChatCompletionMessage{Role: "system", Content: "My name is Castle"})
	return tmpMemory
}
func saveHistory(text string, from string) {
	var myHistory = home + "/." + configAppName + "/" + historyMineFIlename
	var allHistory = home + "/." + configAppName + "/" + historyFileName

	myFh, myFhErr := os.OpenFile(myHistory, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if myFhErr != nil {
		log.Println(myFhErr)
	}
	defer myFh.Close()
	allFh, allFhErr := os.OpenFile(allHistory, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if allFhErr != nil {
		log.Println(allFhErr)
	}
	defer allFh.Close()

	if from == myName {
		if _, err := myFh.WriteString(text + "\n"); err != nil {
			log.Println(err)
		}
	}

	if _, err := allFh.WriteString(from + ": " + text + "\n"); err != nil {
		log.Println(err)
	}

	myFh.Close()
	allFh.Close()
}

/*
func (c *ChatEntry) TypedKey(key *fyne.KeyEvent) {
	if c.Text != "" || len(c.Text) > 0 {
		if key.Name == fyne.KeyReturn || key.Name == fyne.KeyEnter {

		} else {
			// This line processes the key (ie backspace works again)
			c.Entry.TypedKey(key)
		}
		// log.Print(key.Name)
 	}
 }
*/

// Main chat window and functionality
func makeUI() fyne.CanvasObject {
	list := loadMessages()
	var tmpMemory []openai.ChatCompletionMessage
	go func() {
		tmpMemory = getHistoryV2()
	}()

	// msg := widget.NewEntry() - default works
	msg := &ChatEntry{}
	msg.MultiLine = true
	msg.Wrapping = fyne.TextWrapBreak
	msg.ExtendBaseWidget(msg)

	input := container.NewBorder(nil, nil, nil, nil, msg)
	scrollContainer := container.NewVScroll(list)
	content := container.NewBorder(nil, input, nil, nil, scrollContainer)

	send := widget.NewButtonWithIcon("", theme.MailSendIcon(), func() {
		log.Println("---------------------------------------------")
		text := msg.Text
		msg.SetText("")
		list.Add(createMessage(text, myName))
		saveHistory(text, myName)

		// add progres bar and refresh so you can see it
		tmpMsg := widget.NewProgressBarInfinite()
		list.Add(tmpMsg)
		scrollContainer.ScrollToBottom()
		if runtime.GOOS == "darwin" {
			scrollContainer.Refresh()
		}

		tmpMemory = append(tmpMemory, openai.ChatCompletionMessage{Role: "user", Content: text})
		log.Println(tmpMemory)
		resp, _ := apiChatCompletionV2(tmpMemory)
		list.Remove(tmpMsg) // remove progress bar when we have a response

		// Because copy/pasta isn't supported with labels; cheating here
		clipboard.Write(clipboard.FmtText, []byte(resp))
		notification := fyne.NewNotification("Copied!", "Data is in your clipboard")
		a.SendNotification(notification)

		list.Add(createMessage(resp, botName))
		saveHistory(resp, botName)

		// scroll to bottom to see new resposne
		scrollContainer.ScrollToBottom()
		scrollContainer.Refresh()

		speakOn := a.Preferences().Bool("speak")
		if speakOn {
			go speak(resp)
		}
	})

	input = container.NewBorder(nil, nil, nil, send, msg)
	scrollContainer = container.NewVScroll(list)
	content = container.NewBorder(nil, input, nil, nil, scrollContainer)

	return content
}

// ---------------------------------------------------
func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	home, _ = os.UserHomeDir()
	viper.SetConfigName(configFileName)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(home + "/." + configAppName)

	// Create the config directory if it doesn't exist
	_ = os.Mkdir(home+"/."+configAppName, os.ModePerm)

	// Create config file if it doesn't exist
	err := viper.ReadInConfig()
	if err != nil {
		fh, _ := os.Create(home + "/." + configAppName + "/" + configFileName + ".yaml")
		fh.Close()
	}

	// fetch existing value if any
	apiKey = viper.GetString("key")

	// Initialize clipboard copying
	cerr := clipboard.Init()
	if cerr != nil {
		panic(cerr)
	}
}

// This is rendered when the API Key is missing
func windowContentForm(cardBody string) *fyne.Container {
	input := widget.NewEntry()
	input.SetPlaceHolder("Enter Your API Key...")

	content := container.NewVBox(input, widget.NewButton("Save", func() {
		viper.Set("key", input.Text)
		viper.WriteConfig()
		apiKey = input.Text
		input.SetText("")
	}))

	if cardBody == "" {
		cardBody = "Uh oh! You need to enter your OpenAI API Key"
	}
	widgetCard := windowContentCard(cardBody, "")
	link, _ := url.Parse(getAPIDocs)
	widgetLink := widget.NewHyperlink("Find Your API Key", link)

	container := container.New(layout.NewVBoxLayout(), widgetCard, content, widgetLink)

	return container
}

// Updates the window to show that something is happening as the API key is validated
func windowContentProcessing() *fyne.Container {
	cardBodyColor := color.White
	cardSubtitle := "Validating your API Key"
	cardContent := canvas.NewText("", cardBodyColor)
	image := canvas.NewImageFromResource(resourceBenderPng)
	widgetCard := widget.NewCard(configAppName, cardSubtitle, cardContent)
	widgetCard.SetImage(image)

	container := container.New(layout.NewVBoxLayout(), widgetCard, widget.NewProgressBarInfinite())

	return container
}

// TODO cruft that is only called from above method
func windowContentCard(cardBody string, cardSubtitle string) *widget.Card {
	cardBodyColor := color.White
	if cardSubtitle == "" {
		cardSubtitle = "The application is now running in your system tray"
	}
	cardContent := canvas.NewText(cardBody, cardBodyColor)
	image := canvas.NewImageFromResource(resourceBenderPng)
	widgetCard := widget.NewCard(configAppName, cardSubtitle, cardContent)
	widgetCard.SetImage(image)

	return widgetCard
}

// wrapper to call openai just to validate api key
func validateAPIKey(key string) bool {
	var data []openai.ChatCompletionMessage
	data = append(data, openai.ChatCompletionMessage{Role: "sytem", Content: "This is a test to validate the API key"})
	_, err := apiChatCompletionV2(data)
	return err == nil
}

func apiChatCompletionV2(messages []openai.ChatCompletionMessage) (string, error) {
	var responses string

	client := openai.NewClient(apiKey)
	resp, err := client.CreateChatCompletion(
		context.Background(),
		openai.ChatCompletionRequest{
			//Model:       "gpt-3.5-turbo-0613",
			Model:            GPT_MODEL,
			Messages:         messages,
			Temperature:      0.7,
			FrequencyPenalty: 0,
			PresencePenalty:  0,
		},
	)
	if err != nil {
		log.Printf("ChatCompletion error: %v\n", err)
		responses = err.Error()
		return responses, err
	}

	if len(resp.Choices) >= 1 {
		responses = resp.Choices[0].Message.Content
		log.Println(resp)
	}

	return responses, err
}

func speak(text string) {
	var cmd *exec.Cmd
	log.Println(runtime.GOOS)
	if runtime.GOOS == "linux" {
		cmd = exec.Command("espeak", text)

	}
	if runtime.GOOS == "darwin" {
		cmd = exec.Command("say", text)
	}
	out, err := cmd.Output()
	if err != nil {
		log.Println(err)
	}
	log.Println(out)
}

func apiKeyCheck(w fyne.Window) {
	// default show form or not
	if apiKey == "" {
		w.SetContent(windowContentForm(""))
	} else {
		w.SetContent(makeUI())
	}
	w.Show()
}

// Create an About window
func about(a fyne.App) fyne.Window {
	aw := a.NewWindow(configAppName)
	aw.Resize(fyne.NewSize(210, 200))
	widgetCard := windowContentCard("A product of CastleTech", "2023 ver. "+version)
	fyneURL, _ := url.Parse("https://github.com/fyne-io/fyne/blob/master/LICENSE")
	fyneLink := widget.NewHyperlink("FYNE License", fyneURL)
	GolangURL, _ := url.Parse("https://go.dev/LICENSE")
	ChatGPTURL, _ := url.Parse("https://openai.com")
	GolangLink := widget.NewHyperlink("Golang License", GolangURL)
	ChatGPTLink := widget.NewHyperlink("ChatGPT is a product of OpenAI", ChatGPTURL)
	container := container.New(layout.NewVBoxLayout(), widgetCard, fyneLink, GolangLink, ChatGPTLink)
	aw.SetContent(container)

	return aw
}

func main() {
	a = app.NewWithID(appID)
	a.Settings().SetTheme(&myTheme{})
	w := a.NewWindow(configAppName)
	w.SetIcon(resourceBender32x32Png)
	w.Resize(fyne.NewSize(350, 280))

	// desktop app so create system tray
	if desk, ok := a.(desktop.App); ok {
		menuItem1 = fyne.NewMenuItem("Chat", func() {
			apiKeyCheck(w)
		})
		menuItem2 := fyne.NewMenuItem("Setings", nil)
		menuItem2.ChildMenu = fyne.NewMenu("",
			fyne.NewMenuItem("Enable Voice", func() {
				a.Preferences().SetBool("speak", true)
			}),
			fyne.NewMenuItem("Disable Voice", func() {
				a.Preferences().SetBool("speak", false)
			}),
			fyne.NewMenuItem("Delete API Key", func() {
				viper.Set("key", "")
				viper.WriteConfig()
				apiKey = ""
			}),
		)
		menuItem3 := fyne.NewMenuItem("About", func() {
			aw := about(a)
			aw.Show()
		})
		menu = fyne.NewMenu(configAppName, menuItem1, menuItem2, menuItem3)
		icon := resourceBender32x32Png
		desk.SetSystemTrayIcon(icon)
		desk.SetSystemTrayMenu(menu)
	}

	apiKeyCheck(w)

	// when the form is submitted refresh
	// this whole thing needs to be overhauled to not rely on config file change
	viper.OnConfigChange(func(e fsnotify.Event) {
		apiKey = viper.GetString("key")
		if apiKey == "" {
			w.SetContent(windowContentForm(""))
			w.Show()
		} else {
			w.SetContent(windowContentProcessing())
			if validateAPIKey(apiKey) {
				w.SetContent(makeUI())
			} else {
				w.SetContent(windowContentForm("Invalid API Key. Try again."))
			}
		}
	})
	// watch for config changes
	viper.WatchConfig()

	w.SetCloseIntercept(func() {
		w.Hide()
	})
	w.ShowAndRun()
}

// Issues
// widget.Labels can't have custom colors (canvas does - but then can't do resizing easily)
// widget.Labels aren't designed for copy / pasta - fyne.Clipboard only workds with fyne.content
// widget.Entry doesn't support enter key - have to click button
// Doesn't look like all ascii characters will display need to check utf8
